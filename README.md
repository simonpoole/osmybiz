# OpenStreetMap My Business 

Homepage https://osmybiz.osm.ch/ 

## Testing and Local Development

Prototype https://geometalab.gitlab.io/osmybiz .

Run the whole application locally using `docker-compose up` . 
Requires docker and docker-compose for specific instructions per project
 visit the readme.md files in the corresponding folder.
